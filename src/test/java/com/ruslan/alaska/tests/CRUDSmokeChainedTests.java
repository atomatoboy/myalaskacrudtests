package com.ruslan.alaska.tests;

import com.google.gson.Gson;
import com.ruslan.alaska.crud.*;
import com.ruslan.alaska.model.Bear;
import com.ruslan.alaska.model.BearType;
import io.qameta.allure.*;
import io.restassured.response.Response;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import static com.ruslan.alaska.utils.BearStore.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Epic("Here goes Epic name form Allure annotations")
@Feature("Here goes Feature name form Allure annotations")
public class CRUDSmokeChainedTests extends BaseTest {
    @Test
    @Order(1)
    @Description("This is a description for the test named \"testCanGetServiceInfo\" I shall see in allure report")
    @Severity(SeverityLevel.MINOR)
    @Story("Get service info")
    public void testCanGetServiceInfo() {
//  Response response = new ServiceInfo().getDescription(); - is it a nice way to instantiate step class for accessing method in it?
//  Or better to have static methods (like below) to avoid instantiation? Notice that method called only once at all.

        Response response = BearServiceInfo.getDescription();

        assertThat(response.asString(), containsString("This is CRUD service for bears in alaska."));
        Allure.addAttachment("Entire response from GetServiceInfo", response.asString());
    }

    @Test
    @Order(2)
    @Issue("https://siemens-technology.atlassian.net/browse/ELDEV22-48")
    @Story("Create Bear entity")
    public void testCanCreateNewBearEntryAsExpected() {
        Bear bearObjToPost = new Bear(BearType.POLAR, "UMKA", 3.25);

        Response response = BearCreate.newEntry(bearObjToPost);

        assertEquals(bearObjToPost, getOneBearEntryById(bearIdAsResponse(response)),
                "Bear object was not created as expected!");
    }

    @Test
    @Order(3)
    public void testCanCreateBearEntriesSequentially() {
        for (int bearSequentialId = 0,
             numberOfTheBearsCreatedInRow = 4,
             bearIdFromResponse,
             lastBearIdFromResponse = 0;
             bearSequentialId < numberOfTheBearsCreatedInRow; bearSequentialId++) {

            Bear bearObjToPost = new Bear(BearType.BLACK,
                    "NAME FOR A " + (bearSequentialId + 1) + " BEAR", 5.5);

            Response response = BearCreate.newEntry(bearObjToPost);

            bearIdFromResponse = bearIdAsResponse(response);
            if (lastBearIdFromResponse == 0) {
                lastBearIdFromResponse = bearIdFromResponse - 1;
            } //Pass for the first time

            assertEquals(bearObjToPost, getOneBearEntryById(lastBearIdFromResponse + 1));
            lastBearIdFromResponse = bearIdFromResponse;
        }
    }

    @Test
    @Order(4)
    public void testCanCreateBearEntriesOfAllTypes() {
        for (BearType type : BearType.values()) {
            int bearIdFromResponse;
            Bear justCreatedBear;
            String bearNameToGive = "HERE GOES A NAME FOR A BEAR OF " + type.toString()
                    + " TYPE"; //Names saved in capital. Point for one more test.

            Response response = BearCreate.newEntry(new Bear(type, bearNameToGive, 2.2));

            bearIdFromResponse = bearIdAsResponse(response);
            justCreatedBear = getOneBearEntryById(bearIdFromResponse);

            if (justCreatedBear == null) {
                fail("Can not get Bear object for id " + bearIdFromResponse + " with given name: "
                        + bearNameToGive);
            } else {
                assertEquals(
                        bearNameToGive,
                        justCreatedBear.getBear_name(),
                        "Names given and received shall be identical");
            }
        }
    }

    @Test
    @Order(5)
    public void testCanGetListOfAllBearEntries() {
        Response response = BearRead.allEntries();

        List<Bear> bears = response.jsonPath().getList("", Bear.class);

        assertNotNull(bears, "Returned Bears list is NULL!");
    }

    @Test
    @Order(6)
    public void testCanCreateNewBearEntryAndReturnCorrectId() {
        // This test works only in case of adding bears entry on-by-one and list elem index corresponds to elem bear id. Rewrite to handle last entry based on list elem index AND entry id
        // TODO: Make it as test N2

        int highestStoredBearId = getHighestIdOfStoredBears();

        Response response = BearCreate.newEntry(new Bear(BearType.BROWN, "SIMKA", 7.0));

        assertEquals(
                highestStoredBearId + 1,
                bearIdAsResponse(response),
                "Just created Bear entry Id shall be incremented from last bear id");
    }

    @Test
    @Order(7)
    public void testCanGetLastBearByEntryId() {
        int bearIdToGet = getIdOfLastBearFromStore();
        assertTrue(bearIdToGet > 0, "If fail than Bear Entry List is EMPTY - nothing to get");

        Response response = BearRead.entryById(bearIdToGet);

        // if response type would be "application/json" instead of "text/html" then will be possible
        // to deserialize right using restAssured, i.e. Bear bear = response.as(Bear.class);
        // but atm we are getting an error "Cannot parse object because no supported Content-Type was specified in response. Content-Type was 'text/html;charset=utf-8'"
        Bear returnedBearEntry = new Gson().fromJson(response.asString(), Bear.class);
        assertEquals(
                bearIdToGet,
                returnedBearEntry.getBear_id(),
                "Id of the returned Bear entry shall be equal to requested one");
    }

    @Test
    @Order(8)
    public void testCanUpdateLastBearByEntryId() {
        int bearIdToUpdate = getIdOfMidBearInStore();
        Bear srcBearEntry = getOneBearEntryById(bearIdToUpdate);
        String nameToUpdate = srcBearEntry.getBear_name()
                + "..and added substring UPDATED"; //Names saved in lower case. Point for one more test.

        Bear bearObjToUpdate = new Bear(BearType.BLACK, nameToUpdate, 9.99);
        Response response = BearUpdate.existingEntryById(bearObjToUpdate, bearIdToUpdate);

        assertEquals(bearObjToUpdate, getOneBearEntryById(bearIdToUpdate),
                "Bear sent for updated and actually updated are not identical!");
    }

    @Test
    @Order(9)
    public void testCanDeleteBearByEntryId() {
        int bearIdToDelete = getIdOfMidBearInStore();
        int bearEntriesCountBefore = getBearsCountInStore();

        BearDelete.entryById(bearIdToDelete);
        int bearEntriesCountAfter = getBearsCountInStore();

        assertEquals(bearEntriesCountAfter, (bearEntriesCountBefore - 1),
                "Check that Bear entry was actually deleted");
    }

    @Test
    @Order(10)
    public void testCanDeleteAllBearEntries() {
        if (getBearsCountInStore() > 0) {
            BearDelete.allEntries();
            assertEquals(0, getBearsCountInStore(), "Check that all Bear entries was deleted");
        } else {
            fail("Test execution is invalid - nothing to delete as BearsCountInStore is 0!");
        }
    }
}
