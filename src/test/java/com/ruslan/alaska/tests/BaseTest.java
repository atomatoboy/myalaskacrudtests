package com.ruslan.alaska.tests;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

public abstract class BaseTest {

  public static String baseURI = "http://localhost:8080";
  public static RequestSpecification requestSpec;
  public static ResponseSpecification responseSpec;

  @BeforeAll
  public static void SetUp() {
    requestSpec = new RequestSpecBuilder()
            .setBaseUri(baseURI)
            .setContentType(ContentType.JSON)
            .setAccept(ContentType.TEXT)
            .build();
    RestAssured.requestSpecification = requestSpec;

    responseSpec = new ResponseSpecBuilder()
            .expectStatusCode(200)
            .expectContentType("text/html;charset=utf-8")
            .build();
    RestAssured.responseSpecification = responseSpec;
  }

  @AfterAll
  public static void tearDown() {
    //TODO: save last incremented Id for the next test runs in props
  }
}
