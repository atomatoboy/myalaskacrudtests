package com.ruslan.alaska.model;

public enum BearType {
  POLAR {
    public String toString() {
      return "POLAR";
    }
  },
  BROWN {
    public String toString() {
      return "BROWN";
    }
  },
  BLACK {
    public String toString() {
      return "BLACK";
    }
  },
  GUMMY {
    public String toString() {
      return "GUMMY";
    }
  };

  public BearType getNext() {
    return this.ordinal() < BearType.values().length - 1 ? BearType.values()[this.ordinal() + 1]
            : null;
  }

  public BearType getPrev() {
    return this.ordinal() > 0 ? BearType.values()[this.ordinal() - 1] : null;
  }
}