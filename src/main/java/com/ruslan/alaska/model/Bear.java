package com.ruslan.alaska.model;

import java.util.Objects;

public class Bear {

  private final BearType bear_type;
  private final String bear_name;
  private final Double bear_age;
  private Integer bear_id;

  public Bear(Integer bear_id, BearType bear_type, String bear_name, Double bear_age) {
    this.bear_type = bear_type;
    this.bear_name = bear_name;
    this.bear_age = bear_age;
  }

  public Bear(BearType bear_type, String bear_name, Double bear_age) {
    this.bear_type = bear_type;
    this.bear_name = bear_name;
    this.bear_age = bear_age;
  }

  public Integer getBear_id() {
    return bear_id;
  }

  public BearType getBear_type() {
    return bear_type;
  }

  public String getBear_name() {
    return bear_name;
  }

  public Double getBear_age() {
    return bear_age;
  }

  @Override
  public String toString() {
    return "bear_id: " + this.bear_id + ", bear_type: " + this.bear_type.toString()
            + ", bear_name: "
            + this.bear_name + ", bear_age: " + this.bear_age;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Bear bear = (Bear) o;
    return bear_type == bear.bear_type && bear_name.equals(bear.bear_name) && bear_age
            .equals(bear.bear_age);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bear_type, bear_name, bear_age);
  }

}