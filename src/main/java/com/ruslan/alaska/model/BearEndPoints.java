package com.ruslan.alaska.model;

public final class BearEndPoints {

  public static final String info = "/info"; // get service info (get)
  public static final String bear = "/bear"; // get all bears (get), create a new bear (post), delete all bears (delete)
  public static final String bearById = "/bear/{id}"; // get a bear by id (get), updated a bear by id (put), delete a bear by id (delete)
}
