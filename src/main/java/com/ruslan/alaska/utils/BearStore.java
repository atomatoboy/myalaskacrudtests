package com.ruslan.alaska.utils;

import com.google.gson.Gson;
import com.ruslan.alaska.crud.BearRead;
import com.ruslan.alaska.model.Bear;
import io.restassured.response.Response;

import java.util.List;

public class BearStore {

    public static int bearIdAsResponse(Response resp) {
        return Integer.parseInt(resp.asString());
    }

    public static Bear getOneBearEntryById(int bearIdToGet) {
        String responseAsString = BearRead.entryById(bearIdToGet).asString();
        return new Gson().fromJson(responseAsString, Bear.class);
    }

    public static List<Bear> getStoredBearsList() {
        Response allEntriesResponse = BearRead.allEntries();
        return allEntriesResponse.jsonPath().getList("", Bear.class);
    }

    public static int getIdOfLastBearFromStore() {
        List<Bear> bearsList = getStoredBearsList();
        return !bearsList.isEmpty() ? bearsList.get(bearsList.size() - 1).getBear_id() : 0;
    }

    public static int getIdOfMidBearInStore() {
        List<Bear> bearsList = getStoredBearsList();

        return (bearsList.size() % 2) == 0 ? bearsList.get(bearsList.size() / 2).getBear_id()
                : bearsList.get(bearsList.size() / 2 - 1).getBear_id();
    }

    public static int getHighestIdOfStoredBears() {
        int hId = 0;
        List<Bear> bearsList = getStoredBearsList();

        for (Bear bear : bearsList) {
            int curId = bear.getBear_id();
            if (curId > hId) {
                hId = curId;
            }
        }
        return hId;
    }

    public static int getBearsCountInStore() {
        return getStoredBearsList().size();
    }
}