package com.ruslan.alaska.crud;

import com.ruslan.alaska.model.BearEndPoints;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class BearDelete {

  public static Response allEntries() {
    return RestAssured.delete(BearEndPoints.bear);
  }

  public static Response entryById(int bearId) {
    return RestAssured.delete(BearEndPoints.bearById, bearId);
  }

}
