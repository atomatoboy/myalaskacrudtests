package com.ruslan.alaska.crud;

import com.ruslan.alaska.model.BearEndPoints;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class BearRead {


    public static Response allEntries() {
        return RestAssured.get(BearEndPoints.bear);
    }


    public static Response entryById(int bearId) {
        return RestAssured.get(BearEndPoints.bearById, bearId);
    }

}
