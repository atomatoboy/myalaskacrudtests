package com.ruslan.alaska.crud;

import com.ruslan.alaska.model.Bear;
import com.ruslan.alaska.model.BearEndPoints;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class BearUpdate {

  public static Response existingEntryById(Bear bearObj, int bearId) {
    return RestAssured.given()
            .body(bearObj)
            .put(BearEndPoints.bearById, bearId);
  }
}
