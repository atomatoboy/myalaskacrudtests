package com.ruslan.alaska.crud;

import com.ruslan.alaska.model.Bear;
import com.ruslan.alaska.model.BearEndPoints;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class BearCreate {

    public static Response newEntry(Bear bearObj) {
        return RestAssured.given()
                .body(bearObj)
                .post(BearEndPoints.bear);
    }

}
