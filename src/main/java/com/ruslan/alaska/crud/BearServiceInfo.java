package com.ruslan.alaska.crud;

import com.ruslan.alaska.model.BearEndPoints;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class BearServiceInfo {

  public static Response getDescription() {
    return RestAssured.get(BearEndPoints.info);
  }

}