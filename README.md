## Testing task completed by Ruslan Grigoriev 

### Task statement
1. Test REST API Service, which provides CRUD interface.
2. Docker image with tag 1.0 for this service can be obtained by link https://hub.docker.com/r/azshoo/alaska 
3. On container start starting the service app, which available by internal address http://0.0.0.0:8091 
4. Please download docker image and start service app 
5. Design test cases proves that REST API Service is functioning
6. Automate test cases using java
7. All CRUD methods described in response to GET request to endpoint /info

### Solution

1. In order to start service app from downloaded image please run the command *$ docker run --rm --name=alaska
   --publish=8080:8091 azshoo/alaska:1.0*.
2. In order to minimal cover REST API Service is functioning, I was using chained REST API requests, which tests all CRUD methods
provided by service, i.e:
---
- Get service info: GET /info
- Get all bears: GET /bear
- Get specific bear by id: GET /bear/{id}
- Create a bear: POST /bear
- Update a bear: PUT /bear
- Delete specific bear by id: DELETE /bear/{id}
- Delete all bears: GET /bear
---

### Tech stack used for automation

1. OpenJDK 8 /1.8.0_232
2. JUnit 5 with Hamcrest Assertions
3. Rest Assured test framework + Google GSON.

## Test run results

10 total, 2 failed, 8 passed

- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Passed - testCanGetServiceInfo()
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Passed -
  testCanCreateNewBearEntryAsExpected()
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Passed -
  testCanCreateBearEntriesSequentially()
- ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) Failed -
  testCanCreateBearEntriesOfAllTypes()
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Passed -
  testCanGetListOfAllBearEntries()
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Passed -
  testCanCreateNewBearEntryAndReturnCorrectId()
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Passed -
  testCanGetLastBearByEntryId()
- ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) Failed -
  testCanUpdateLastBearByEntryId()
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Passed -
  testCanDeleteBearByEntryId()
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Passed -
  testCanDeleteAllBearEntries()

## Notes

Please note, that this test suite is *Smoke* and has *Minimal* test coverage for ensure CRUD service
is functioning at all. It easily can be extended to test negative tes cases, boundary values,
combinations of test data etc. 